package com.smasly.eventdriven;

import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.smasly.eventdriven.api.EnableSmaslyEventDriven;
import com.smasly.eventdriven.api.TestEventHandler;
import com.smasly.eventdriven.api.TestMessageQueueTemplate;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

@Configuration
@EnableSmaslyEventDriven
public class TestConfiguration {

    @Bean
    public TestMessageQueueTemplate testMessageQueueTemplate() {
        return new TestMessageQueueTemplate();
    }

    @Bean
    public TestEventHandler testEventHandler() {
        return new TestEventHandler();
    }

    @Bean
    public ObjectMapper objectMapper(@Qualifier("eventApiModule") Module eventApiModule) {
        ObjectMapper objectMapper = Jackson2ObjectMapperBuilder.json().build();
        objectMapper.registerModule(eventApiModule);
        return objectMapper;
    }

}
