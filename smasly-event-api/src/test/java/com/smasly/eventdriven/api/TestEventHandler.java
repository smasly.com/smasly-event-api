package com.smasly.eventdriven.api;

import com.smasly.eventdriven.api.handler.EventHandler;
import com.smasly.eventdriven.api.model.EventCorrelationDto;

public class TestEventHandler implements EventHandler<TestEventModel> {

    @Override
    public void handleEvent(EventCorrelationDto correlation, TestEventModel event) {
        System.out.println("Event handled");
    }
}
