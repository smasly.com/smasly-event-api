package com.smasly.eventdriven.api.model;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.smasly.eventdriven.TestConfiguration;
import com.smasly.eventdriven.api.EventDrivenAutoConfiguration;
import com.smasly.eventdriven.api.TestEventModel;
import com.smasly.eventdriven.api.exception.EventHandlerNotFoundException;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.temporal.ChronoField;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {TestConfiguration.class, EventDrivenAutoConfiguration.class})
public class ResponseDtoDeserializerTest {

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void objectMapperConfigured_deserializeMessage_success() throws IOException {
        // given
        File file = new ClassPathResource("json/testEventModel.json").getFile();
        // then
        MessageBusEvent message = objectMapper.readerFor(MessageBusEvent.class).readValue(file);
        assertThat(message.getEventCode()).isEqualTo("TestEventModel");
        assertThat(message.getPayload()).isNotNull();
        assertThat(message.getCorrelationData().getQuery()).isEqualTo("test query");
        assertThat(message.getCreatedServiceName()).isEqualTo("smasly.com");
        assertThat(message.getCreatedDate()).isEqualTo(Instant.now().atZone(ZoneOffset.UTC).with(ChronoField.YEAR, 2019)
                .with(ChronoField.MONTH_OF_YEAR, 9).with(ChronoField.DAY_OF_MONTH, 27).with(ChronoField.HOUR_OF_DAY, 15)
                .with(ChronoField.MINUTE_OF_HOUR, 38).with(ChronoField.SECOND_OF_MINUTE, 13).with(ChronoField.NANO_OF_SECOND, 539000000)
                .toInstant());
    }

    @Test
    public void objectMapperConfigured_serializeMessage_noCodePropertyUnderPayload() throws IOException {
        // given
        TestEventModel testEventModel = new TestEventModel("value");
        MessageBusEvent messageBusEvent = new MessageBusEvent<>(testEventModel);
        // then
        String json = objectMapper.writeValueAsString(messageBusEvent);
        JsonNode codeProperty = objectMapper.readTree(json).get("payload").get("code");
        assertThat(codeProperty).isNull();
        String eventCode = objectMapper.readTree(json).get("eventCode").asText();
        assertThat(eventCode).isEqualTo(testEventModel.getCode());
    }

    @Test
    public void objectMapperConfigured_unexistedEvent_eventHandlerNotFoundException() throws IOException {
        // given
        File file = new ClassPathResource("json/unexistingEvent.json").getFile();
        // then
        JsonParseException expectedException = Assertions.assertThrows(JsonParseException.class,
                () -> objectMapper.readerFor(MessageBusEvent.class).readValue(file));
        Assertions.assertEquals(EventHandlerNotFoundException.class, expectedException.getCause().getClass());
    }

}
