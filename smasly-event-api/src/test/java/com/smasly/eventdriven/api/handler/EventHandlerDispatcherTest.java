package com.smasly.eventdriven.api.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.smasly.eventdriven.TestConfiguration;
import com.smasly.eventdriven.api.EventDrivenAutoConfiguration;
import com.smasly.eventdriven.api.TestEventHandler;
import com.smasly.eventdriven.api.TestEventModel;
import com.smasly.eventdriven.api.exception.EventHandlerException;
import com.smasly.eventdriven.api.model.MessageBusEvent;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.IOException;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {TestConfiguration.class, EventDrivenAutoConfiguration.class})
public class EventHandlerDispatcherTest {

    @Autowired
    private EventHandlerDispatcher eventHandlerDispatcher;

    @SpyBean
    private TestEventHandler testEventHandler;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void simulateMessage_dispatchEvent_testEventHandlerInvoked() throws IOException, EventHandlerException {
        // given
        File file = new ClassPathResource("json/testEventModel.json").getFile();
        MessageBusEvent<?> message = objectMapper.readerFor(MessageBusEvent.class).readValue(file);
        eventHandlerDispatcher.handleMessage(message);
        // then
        Mockito.verify(testEventHandler).handleEvent(message.getCorrelationData(), (TestEventModel) message.getPayload());
    }

}
