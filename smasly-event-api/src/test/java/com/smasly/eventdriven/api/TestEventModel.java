package com.smasly.eventdriven.api;

import com.smasly.eventdriven.api.model.CodeAwareEvent;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class TestEventModel implements CodeAwareEvent {

    private String property;

    @Override
    public String getCode() {
        return "TestEventModel";
    }
}
