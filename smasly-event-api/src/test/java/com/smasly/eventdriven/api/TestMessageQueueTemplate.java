package com.smasly.eventdriven.api;

import com.smasly.eventdriven.api.sender.MessageQueueTemplate;

public class TestMessageQueueTemplate implements MessageQueueTemplate {

    @Override
    public void convertAndSend(String exchange, String routingKey, Object message) {
        System.out.println("Message converted and sent");
    }
}
