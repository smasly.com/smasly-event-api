package com.smasly.eventdriven.api;

import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.smasly.eventdriven.api.handler.EventHandlerDispatcher;
import com.smasly.eventdriven.api.handler.EventHandlerResolver;
import com.smasly.eventdriven.api.model.MessageBusEvent;
import com.smasly.eventdriven.api.model.ResponseDtoDeserializer;
import com.smasly.eventdriven.api.sender.MessageQueueTemplate;
import com.smasly.eventdriven.api.sender.MessageSender;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
@ConditionalOnBean(value = {MessageQueueTemplate.class}, annotation = EnableSmaslyEventDriven.class)
public class EventDrivenAutoConfiguration {

    private final ConfigurableListableBeanFactory beanFactory;

    /**
     * Register custom deserializer for event DTO
     */
    @Bean
    public Module eventApiModule(EventHandlerResolver eventHandlerResolver) {
        return new SimpleModule()
                .addDeserializer(MessageBusEvent.class, new ResponseDtoDeserializer(eventHandlerResolver));
    }

    @Bean
    EventHandlerResolver eventHandlerResolver() {
        return new EventHandlerResolver(beanFactory);
    }

    @Bean
    EventHandlerDispatcher eventHandlerDispatcher(EventHandlerResolver eventHandlerResolver) {
        return new EventHandlerDispatcher(eventHandlerResolver, beanFactory);
    }

    @Bean
    MessageSender messageSender(MessageQueueTemplate messageQueueTemplate, ApplicationContext applicationContext) {
        return new MessageSender(messageQueueTemplate, applicationContext);
    }

}
