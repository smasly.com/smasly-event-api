package com.smasly.eventdriven.api.handler;


import com.smasly.eventdriven.api.model.CodeAwareEvent;
import com.smasly.eventdriven.api.model.EventCorrelationDto;

/**
 * Handle events from other services.
 * Implementation also could create events for it's own async processing.
 *
 * @param <E> Event payload
 */
public interface EventHandler<E extends CodeAwareEvent> {

    // todo add logs e.g. One event with code 'XXX' is handled by 'this.getClass().getSimpleName()'
    void handleEvent(EventCorrelationDto correlation, E event);

}
