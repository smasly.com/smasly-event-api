package com.smasly.eventdriven.api.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EventCorrelationDto {

    /**
     * If not null, it must be set in the response event as {@link MessageBusEvent#eventCode event code}.
     * It is responsibility of client to set it.
     */
    private String expectedResponseCode;

    /**
     * If not null, there was search request for tweets with such query.
     */
    private String query;

    /**
     * If not null, there was search request for tweets with hashtag.
     */
    private String hashTagDataId;

    /**
     * If not null, there was user timeline request specified by this twitter screen name.
     */
    private String twitterScreenName;
}
