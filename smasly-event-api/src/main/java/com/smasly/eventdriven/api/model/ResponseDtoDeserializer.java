package com.smasly.eventdriven.api.model;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.smasly.eventdriven.api.exception.EventHandlerNotFoundException;
import com.smasly.eventdriven.api.handler.EventHandlerResolver;
import lombok.RequiredArgsConstructor;

import java.io.IOException;
import java.time.Instant;

/**
 * Custom deserializer to provide generic payload in the message.
 */
@RequiredArgsConstructor
public class ResponseDtoDeserializer extends JsonDeserializer<MessageBusEvent> {

    private final EventHandlerResolver eventHandlerResolver;

    @Override
    public MessageBusEvent deserialize(JsonParser parser, DeserializationContext context) throws IOException {
        JsonNode root = parser.readValueAs(JsonNode.class);
        EventCorrelationDto correlation = readValue(parser, context, root.get("correlationData"), EventCorrelationDto.class);
        String eventCode = root.path("eventCode").asText();
        Class<? extends CodeAwareEvent> eventType;
        try {
            eventType = eventHandlerResolver.getEventTypeByCode(eventCode);
        } catch (EventHandlerNotFoundException e) {
            throw new JsonParseException(parser, "Event could not be deserialize due to exception", e);
        }
        CodeAwareEvent result = readValue(parser, context, root.get("payload"), eventType);
        MessageBusEvent message = new MessageBusEvent<>(result);
        message.setCorrelationData(correlation);
        message.setEventCode(eventCode);
        message.setCreatedServiceName(root.path("createdServiceName").asText());
        Instant createdDate = readValue(parser, context, root.get("createdDate"), Instant.class);
        message.setCreatedDate(createdDate);
        return message;
    }

    private <T> T readValue(JsonParser parser, DeserializationContext context, JsonNode node, Class<T> type)
            throws IOException {
        JsonParser p = node.traverse(parser.getCodec());
        p.nextToken();
        return context.readValue(p, type);
    }

}
