package com.smasly.eventdriven.api.handler;

import com.smasly.eventdriven.api.exception.EventHandlerException;
import com.smasly.eventdriven.api.model.CodeAwareEvent;
import com.smasly.eventdriven.api.model.MessageBusEvent;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

@RequiredArgsConstructor
public class EventHandlerDispatcher {

    private final EventHandlerResolver eventHandlerResolver;
    private final ConfigurableListableBeanFactory beanFactory;

    public void handleMessage(MessageBusEvent<? extends CodeAwareEvent> message) throws EventHandlerException {
        String beanName = eventHandlerResolver.getEventHandlerBeanNameByCode(message.getEventCode());
        EventHandler handler = beanFactory.getBean(beanName, EventHandler.class);
        handler.handleEvent(message.getCorrelationData(), message.getPayload());
    }

}
