package com.smasly.eventdriven.api.sender;

public interface MessageQueueTemplate {

    /**
     * Convert a Java object to an message and send it to a specific exchange
     * with a specific routing key via specific message queue.
     *
     * @param exchange   the name of the exchange
     * @param routingKey the routing key
     * @param message    a message to send
     */
    void convertAndSend(String exchange, String routingKey, Object message);

}
