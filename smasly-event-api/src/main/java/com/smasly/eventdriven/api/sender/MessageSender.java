package com.smasly.eventdriven.api.sender;

import com.smasly.eventdriven.api.model.CodeAwareEvent;
import com.smasly.eventdriven.api.model.EventCorrelationDto;
import com.smasly.eventdriven.api.model.MessageBusEvent;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationContext;

@RequiredArgsConstructor
public class MessageSender {

    private final MessageQueueTemplate messageQueueTemplate;
    private final ApplicationContext applicationContext;

    public void convertAndSend(String exchange, String routingKey, final CodeAwareEvent event, EventCorrelationDto eventCorrelationDto) {
        MessageBusEvent messageToSend = createEvent(event, eventCorrelationDto);
        messageQueueTemplate.convertAndSend(exchange, routingKey, messageToSend);
    }

    private MessageBusEvent createEvent(CodeAwareEvent event, EventCorrelationDto eventCorrelationDto) {
        MessageBusEvent messageBusEvent = new MessageBusEvent<>(event);
        messageBusEvent.setCorrelationData(eventCorrelationDto);
        messageBusEvent.setCreatedServiceName(applicationContext.getId());
        return messageBusEvent;
    }
}
