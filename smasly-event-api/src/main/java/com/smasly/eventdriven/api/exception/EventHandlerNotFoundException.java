package com.smasly.eventdriven.api.exception;

import lombok.Getter;

@Getter
public class EventHandlerNotFoundException extends EventHandlerException {

    private final String eventCode;

    public EventHandlerNotFoundException(String eventCode) {
        super("Event handler not found for event code " + eventCode);
        this.eventCode = eventCode;
    }

}
