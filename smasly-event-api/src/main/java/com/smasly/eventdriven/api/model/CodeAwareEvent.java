package com.smasly.eventdriven.api.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Base interface for all events.
 * Each event is identified by it's code.
 * Event must have default constructor.
 */
public interface CodeAwareEvent {

    /**
     * By default, event code is event model class name
     *
     * @return Event code
     */
    @JsonIgnore
    default String getCode() {
        return this.getClass().getSimpleName();
    }
}
