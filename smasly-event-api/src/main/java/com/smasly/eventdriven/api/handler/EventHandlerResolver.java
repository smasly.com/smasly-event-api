package com.smasly.eventdriven.api.handler;

import com.smasly.eventdriven.api.exception.EventHandlerNotFoundException;
import com.smasly.eventdriven.api.exception.EventHandlerNotResolvedException;
import com.smasly.eventdriven.api.model.CodeAwareEvent;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.AnnotatedBeanDefinition;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.GenericTypeResolver;
import org.springframework.core.type.MethodMetadata;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

/**
 * Auto configuration to resolve all events available in microservice
 */
@RequiredArgsConstructor
public class EventHandlerResolver implements ApplicationListener<ContextRefreshedEvent> {

    private final ConfigurableListableBeanFactory beanFactory;
    private Map<String, String> eventCodeToEventHandlerBeanName = new HashMap<>();
    private Map<String, Class<? extends CodeAwareEvent>> eventCodeToEventType = new HashMap<>();

    public String getEventHandlerBeanNameByCode(String eventCode) throws EventHandlerNotFoundException {
        String beanName = eventCodeToEventHandlerBeanName.get(eventCode);
        if (beanName == null) {
            throw new EventHandlerNotFoundException(eventCode);
        }
        return beanName;
    }

    public Class<? extends CodeAwareEvent> getEventTypeByCode(String eventCode) throws EventHandlerNotFoundException {
        Class<? extends CodeAwareEvent> eventType = eventCodeToEventType.get(eventCode);
        if (eventType == null) {
            throw new EventHandlerNotFoundException(eventCode);
        }
        return eventType;
    }

    @SneakyThrows
    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshEvent) {
        resolveEventHandlers();
    }

    /**
     * Find all beans with type {@link EventHandler} and register them to {@link #eventCodeToEventHandlerBeanName} and
     * {@link #eventCodeToEventType} maps.
     */
    private void resolveEventHandlers() throws EventHandlerNotResolvedException {
        eventCodeToEventHandlerBeanName.clear();
        eventCodeToEventType.clear();
        String[] handlerNames = beanFactory.getBeanNamesForType(EventHandler.class);
        for (String beanName : handlerNames) {
            BeanDefinition handler = beanFactory.getBeanDefinition(beanName);
            Class<?> handlerClass = null;
            Class<CodeAwareEvent> handledType = null;
            String handledEventCode = null;
            try {
                handlerClass = Class.forName(getBeanClassName(handler));
                Class<?> beanTypeArgument = GenericTypeResolver.resolveTypeArgument(handlerClass, EventHandler.class);
                if (beanTypeArgument != null && CodeAwareEvent.class.isAssignableFrom(beanTypeArgument)) {
                    handledType = (Class<CodeAwareEvent>) beanTypeArgument;
                } else {
                    throw new EventHandlerNotResolvedException(handlerClass,
                            "Event type " + beanTypeArgument + " must implement " + CodeAwareEvent.class.getName());
                }
                Constructor<CodeAwareEvent> constructor = handledType.getConstructor();
                handledEventCode = constructor.newInstance().getCode();
            } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
                throw new EventHandlerNotResolvedException(handlerClass, "Event type " + handledType + " must have default constructor.");
            } catch (ClassNotFoundException e) {
                // It's impossible to reach here. Bean class always exist.
            }
            eventCodeToEventHandlerBeanName.put(handledEventCode, beanName);
            eventCodeToEventType.put(handledEventCode, handledType);
        }
    }

    /**
     * Supports bean definition in {@link org.springframework.context.annotation.Configuration @Configuration} classes
     * or using {@link org.springframework.stereotype.Component @Component} / {@link org.springframework.stereotype.Service @Service}
     * annotations
     */
    private String getBeanClassName(BeanDefinition handler) {
        if (handler instanceof AnnotatedBeanDefinition) {
            MethodMetadata factoryMethodMetadata = ((AnnotatedBeanDefinition) handler).getFactoryMethodMetadata();
            if (factoryMethodMetadata != null) {
                return factoryMethodMetadata.getReturnTypeName();
            }
        }
        return handler.getBeanClassName();
    }

}
