package com.smasly.eventdriven.api.exception;

public class EventHandlerNotResolvedException extends EventHandlerException {

    private static final String ADDITIONAL_MESSAGE = "It is impossible to resolve event handler from class %s";

    public EventHandlerNotResolvedException(Class<?> handlerClass, String message) {
        super(message + String.format(ADDITIONAL_MESSAGE, handlerClass));
    }
}
