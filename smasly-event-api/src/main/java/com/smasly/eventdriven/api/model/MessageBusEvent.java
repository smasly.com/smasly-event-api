package com.smasly.eventdriven.api.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.time.Instant;

@Getter
@Setter
@RequiredArgsConstructor
public class MessageBusEvent<E extends CodeAwareEvent> {

    /**
     * Core data of the event
     */
    private E payload;

    /**
     * Unique code of the event. It determines {@link #payload} structure.
     */
    private String eventCode;

    /**
     * Object that contains correlation data.
     * It will be returned as unchanged in the responded event (in case there is responded event).
     */
    private EventCorrelationDto correlationData;

    private String createdServiceName;
    private Instant createdDate = Instant.now();

    public MessageBusEvent(E payload) {
        this.payload = payload;
        this.eventCode = payload.getCode();
    }

}
